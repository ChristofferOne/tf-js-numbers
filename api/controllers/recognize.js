const tf = require('@tensorflow/tfjs-node')
const model = require('./../../lib/ML/model')
const { createCanvas, Image } = require('canvas')

const predict = async (req, res, next) => {
  const { data: jsonData } = req.body

  if (!jsonData) {
    res.status(400).json('`data` parameter is required')
  }

  const data = JSON.parse(jsonData);

  const canvas = createCanvas(28, 28)
  const ctx = canvas.getContext('2d')

  const image = new Image();

  image.onload = () => {
    ctx.drawImage(image, 0, 0)
  }
  image.onerror = err => console.log(err)

  image.src = data.image;

  let tensor = tf.fromPixels(canvas, 1)
  tensor = tensor.reshape([1, 28, 28, 1])
  tensor = tf.cast(tensor, 'float32')

  const result = await model.predict(tensor)

  res.json(result)

  next()
}

module.exports = { predict }
