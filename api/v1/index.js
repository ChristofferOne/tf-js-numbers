const express = require('express')
const router = express.Router()

const { health } = require('../controllers/health')
const { predict } = require('../controllers/recognize')

router.get('/health', health)
router.post('/recognize', predict)

module.exports = router
