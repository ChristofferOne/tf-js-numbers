const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')

const model = require('./lib/ML/model')

const router = require('./api')

const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(router)

const init = async () => {
  await model.make()
  await model.save()
}

fs.readdir(`${__dirname}/lib/ML/models`, (err, files) => {
  if (err) {
    console.log('Failed to load model.')
    console.log(err)
  } else {
    if (!files.length) {
      _ = init()
    }
  }
})

app.listen(port, () => console.log(`Running on port ${port}`))
