const tf = require('@tensorflow/tfjs-node')

const generateModel = async () => {
  let model

  console.log('Model generation started...')

  model = tf.sequential()

  console.log('Adding model layers...')

  model.add(tf.layers.conv2d({
    inputShape: [28, 28, 1],
    kernelSize: 5,
    filters: 8,
    strides: 1,
    activation: 'relu',
    kernelInitializer: 'VarianceScaling'
  }))

  model.add(tf.layers.maxPooling2d({
    poolSize: [2, 2],
    strides: [2, 2]
  }))

  model.add(tf.layers.conv2d({
    kernelSize: 5,
    filters: 16,
    strides: 1,
    activation: 'relu',
    kernelInitializer: 'VarianceScaling'
  }))

  model.add(tf.layers.maxPooling2d({
    poolSize: [2, 2],
    strides: [2, 2]
  }))

  model.add(tf.layers.flatten())

  model.add(tf.layers.dense({
    units: 10,
    kernelInitializer: 'VarianceScaling',
    activation: 'softmax'
  }))

  console.log('Layers created')

  console.log('Starting compilation...')

  await model.compile({
    optimizer: tf.train.sgd(0.15),
    loss: 'categoricalCrossentropy'
  })

  console.log('Model compiled')
  console.log('Model ready')

  return model
}

module.exports = { generateModel }
