const MnistData = require('./data')

const load = async () => {
  console.log('Started loading data...')

  const data = MnistData

  await data.loadData()

  console.log('Data loading completed')

  return data
}

module.exports = { load }
