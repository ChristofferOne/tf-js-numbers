const tf = require('@tensorflow/tfjs-node')
const generator = require('./generator')
const training = require('./training')

let model

const make = async () => {
  this.model = await generator.generateModel()
  this.model = await training.train(model)
}

const save = async () => {
  console.log('Starting export of model...')

  await this.model.save(`file://${__dirname}/models`)

  console.log('Model export completed')
}

const load = async () => {
  console.log('Loading model...')

  this.model = await tf.loadModel(`file://${__dirname}/models/model.json`)

  console.log('Model loaded')
}

const predict = async (img) => {
  _ = await load()

  return tf.tidy(() => {
    return this.model.predict(img).dataSync()
  })
}

module.exports = { make, save, predict }
